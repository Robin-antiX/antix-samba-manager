# antiX SAMBA manager

Script for managing SAMBA shares in antiX control centre.

## Manual installation:

Copy the three script files from bin into _/usr/local/bin_ folder of your system and make them executable:
- antiX-samba-mgr
- antiX-mount-samba-shares
- antiX-setup-samba-shares

Make sure the dependencies are installed:
- yad
- cifs-utils
- smbclient
- the icon files _antiX-smb-mgr01.png_ ... _antiX-smb-mgr06.png_ are available and registered as named GTK icon _antiX-smb-mgr01_ ... _antiX-smb-mgr06_.

## Usage
Either click on the menu entry in antiX control centre, or enter _antiX-samba-mgr_ in a command line window, e.g. in RoxTerm.

## Support
For help, questions, suggestions and bug reporting please write to [antiXlinux forums](https://www.antixforum.com).

## Contributing
- If you find a translation of user interface or user manual being inadequat or misleading wrong, please edit it simply on [antix-translations at transifex](https://www.transifex.com/antcapitalista/antix-development/). On transifex you may edit the string translations used in user interface directly. Your improvements will get included into next update of the package.

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

-----------
Written 2024 by Robin.antiX for antiX community.


